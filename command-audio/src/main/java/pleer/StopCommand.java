package pleer;

import javax.swing.*;
import java.io.File;

public class StopCommand extends MusicCommands {

    public StopCommand(int currentTrack, File[] fileList, Track track, JLabel labelPlay) {
        super(currentTrack, fileList, track, labelPlay);
    }

    @Override
    public Boolean execute() {
        if (track != null) {
            track.stop();
            track = null;
            return true;
        }
        return false;
    }

}
