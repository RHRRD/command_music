package pleer;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;

public class OpenCommand extends MusicCommands {

    private Component parent;

    public OpenCommand(int currentTrack, File[] fileList, Track track, JLabel labelPlay, Component parent) {
        super(currentTrack, fileList, track, labelPlay);
        this.parent = parent;
    }

    @Override
    public Boolean execute() {
        currentTrack = 0;
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("Music Files", "mp3"));
        fc.setCurrentDirectory(new File(getClass().getClassLoader().getResource("music/").getFile()));
        fc.setMultiSelectionEnabled(true);
        fc.showOpenDialog(parent);
        fileList = fc.getSelectedFiles();
        if (fileList != null && fileList.length != 0) {
            play();
            return true;
        }
        return false;
    }
}
