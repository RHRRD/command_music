package pleer;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.advanced.AdvancedPlayer;
import javazoom.jl.player.advanced.PlaybackListener;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Track extends PlaybackListener implements Runnable {

    private String track;
    private AdvancedPlayer player;
    private Thread thread;

    public Track(String track) {
        this.track = track;
    }

    public Track(File file) {
        this.track = file.getName();
    }

    public void play() {
        try {
            player = new AdvancedPlayer(new java.net.URL(track).openStream(), javazoom.jl.player.FactoryRegistry.systemRegistry().createAudioDevice());
            player.setPlayBackListener(this);
            thread = new Thread(this, "AudioPlayThread");
            thread.start();
        } catch (MalformedURLException ex) {
            Logger.getLogger(Track.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JavaLayerException ex) {
            Logger.getLogger(Track.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Track.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void stop() {
        player.stop();
    }

    @Override
    public void run() {
        try {
            player.play();
        } catch (JavaLayerException ex) {
            Logger.getLogger(Track.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
