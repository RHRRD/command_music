package pleer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class Frame extends JFrame {

    private JFrame mainFrame;
    private File[] fileList;
    private int currentTrack;
    private Track track;
    private JLabel labelPlay;

    public Frame() throws HeadlessException {
        initFrame();
    }

    private void initFrame() {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        mainFrame = new JFrame("APl");
        mainFrame.setSize(275, 185);
        JLabel labelTitle = new JLabel("Now playing:");
        labelTitle.setHorizontalAlignment(JLabel.CENTER);
        labelTitle.setVerticalAlignment(JLabel.CENTER);
        labelPlay = new JLabel("nothing");
        labelPlay.setHorizontalAlignment(JLabel.CENTER);
        labelPlay.setVerticalAlignment(JLabel.CENTER);

        JButton buttOpen = new JButton("Open");
        JButton buttStop = new JButton("Stop");
        JButton buttNext = new JButton("Next");
        JButton buttMix = new JButton("Mix");

        mainFrame.setLayout(new GridLayout(4, 1));
        mainFrame.add(labelTitle);
        mainFrame.add(labelPlay);
        JPanel tmpPanel = new JPanel();
        mainFrame.add(tmpPanel);
        JPanel ttt = new JPanel();
        mainFrame.add(ttt);
        ttt.add(buttOpen);

        tmpPanel.add(buttNext, BorderLayout.WEST);
        tmpPanel.add(buttMix, BorderLayout.CENTER);
        tmpPanel.add(buttStop, BorderLayout.EAST);

        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setResizable(false);
        mainFrame.setVisible(true);

        MusicCommands openCommand = new OpenCommand(currentTrack, fileList, track, labelPlay, Frame.this);
        MusicCommands mixCommand = new MixCommand(currentTrack, fileList, track, labelPlay);
        MusicCommands stopCommand = new StopCommand(currentTrack, fileList, track, labelPlay);
        MusicCommands nextCommand = new NextCommand(currentTrack, fileList, track, labelPlay);

        buttOpen.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                openCommand.execute();
            }
        });
        buttNext.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                nextCommand.execute();
            }
        });
        buttStop.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                stopCommand.execute();
            }
        });
        buttMix.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                mixCommand.execute();
            }
        });
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new Frame();
            }
        });
    }
}
