package pleer;

import javax.swing.*;
import java.io.File;

public abstract class MusicCommands {

    protected static int currentTrack;
    protected static File[] fileList;
    protected static Track track;
    protected static JLabel labelPlay;

    public MusicCommands(int currentTrack, File[] fileList, Track track, JLabel labelPlay) {
        this.currentTrack = currentTrack;
        this.fileList = fileList;
        this.track = track;
        this.labelPlay = labelPlay;
    }

    public abstract Boolean execute();

    protected void play() {
        if (track != null) {
            track.stop();
        }
        String ur = "file:///" + fileList[currentTrack].getAbsolutePath();
        labelPlay.setText(fileList[currentTrack].getName());
        track = new Track(ur);
        track.play();
    }

}
