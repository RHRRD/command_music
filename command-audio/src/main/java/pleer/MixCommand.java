package pleer;

import javax.swing.*;
import java.io.File;
import java.util.Random;

public class MixCommand extends MusicCommands {

    public MixCommand(int currentTrack, File[] fileList, Track track, JLabel labelPlay) {
        super(currentTrack, fileList, track, labelPlay);
    }

    @Override
    public Boolean execute() {
        currentTrack = 0;
        if (fileList != null && fileList.length != 0) {
            mixList();
            play();
            return true;
        }
        return false;
    }

    private void mixList() {
        Random rd = new Random();
        File tmp;
        for (int i = 0, j; i < fileList.length; i++) {
            j = rd.nextInt(fileList.length);
            tmp = fileList[j];
            fileList[j] = fileList[i];
            fileList[i] = tmp;
        }
    }

}
