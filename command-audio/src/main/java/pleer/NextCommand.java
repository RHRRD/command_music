package pleer;

import javax.swing.*;
import java.io.File;

public class NextCommand extends MusicCommands {

    public NextCommand(int currentTrack, File[] fileList, Track track, JLabel labelPlay) {
        super(currentTrack, fileList, track, labelPlay);
    }

    @Override
    public Boolean execute() {
        if (fileList != null) {
            currentTrack = currentTrack < fileList.length - 1 ? currentTrack + 1 : 0;
            play();
            return true;
        }
        return false;
    }
}
